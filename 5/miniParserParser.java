/* miniParserParser.java */
/* Generated By:JavaCC: Do not edit this line. miniParserParser.java */
    public class miniParserParser implements miniParserParserConstants {
        public static SymbolTabelle st = new SymbolTabelle();
        public static ByteCodeBuilder bcb = new ByteCodeBuilder();
        public static JavaClassFileGenerator jcfg = new JavaClassFileGenerator("out", true, true, true);
        public static void main (String args[]) throws SymbolAlreadyDefinedException, UnknownSymbolException {
            String byteCode = "";
            miniParserParser parser= new miniParserParser (System.in);
            try {
                byteCode = parser.start();
            } catch (ParseException e) {
                System.err.println(e);
            }
            MethodObject mo = new MethodObject("main",0,byteCode);
            jcfg.generateClassFile(mo);
        }

  static final public String start() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {String a;
    a = programm();
/* System.out.println */ {if ("" != null) return (a + bcb.reTurn()) ;}
    throw new Error("Missing return statement in function");
  }

  static final public String programm() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {String a = "";
    String b = "";
    constDecl();
    a = varDecl();
    b = statement();
{if ("" != null) return a + b;}
    throw new Error("Missing return statement in function");
  }

  static final public void constDecl() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case FINAL:{
      jj_consume_token(FINAL);
      jj_consume_token(INT);
      constZuw();
      constList();
      jj_consume_token(14);
      break;
      }
    default:
      jj_la1[0] = jj_gen;
      ;
    }
  }

  static final public void constZuw() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {Token a;
    Token b;
    a = jj_consume_token(IDENT);
    jj_consume_token(15);
    b = jj_consume_token(NUMBER);
st.AddKonstante(a.image,b.image);
  }

  static final public void constList() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 16:{
      jj_consume_token(16);
      constZuw();
      constList();
      break;
      }
    default:
      jj_la1[1] = jj_gen;
      ;
    }
  }

  static final public String varDecl() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {String a = "";
    String b = "";
    Token c;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case INT:{
      jj_consume_token(INT);
      c = jj_consume_token(IDENT);
      a = varZuw(c.image);
      b = varList();
      jj_consume_token(14);
      break;
      }
    default:
      jj_la1[2] = jj_gen;
      ;
    }
{if ("" != null) return a + b;}
    throw new Error("Missing return statement in function");
  }

  static final public String varZuw(String ident) throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {Token a = null;
    String c = "";
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 15:{
      jj_consume_token(15);
      a = jj_consume_token(NUMBER);
      break;
      }
    default:
      jj_la1[3] = jj_gen;
      ;
    }
if (a != null){
            c = st.AddVariable(ident,Integer.parseInt(a.image));
        } else {
            c = st.AddVariable(ident,0);
        }
        {if ("" != null) return c;}
    throw new Error("Missing return statement in function");
  }

  static final public String varList() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {Token t;
    String a = "";
    String b = "";
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 16:{
      jj_consume_token(16);
      t = jj_consume_token(IDENT);
      a = varZuw(t.image);
      b = varList();
      break;
      }
    default:
      jj_la1[4] = jj_gen;
      ;
    }
{if ("" != null) return a + b;}
    throw new Error("Missing return statement in function");
  }

  static final public String statement() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {String a;
    String b;
    String c;
    Token t;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case IDENT:{
      t = jj_consume_token(IDENT);
      jj_consume_token(15);
      a = expression();
      jj_consume_token(14);
{if ("" != null) return a + bcb.istore(st.GetAdress(t.image));}
      break;
      }
    case PRINT:{
      jj_consume_token(PRINT);
      jj_consume_token(17);
      a = expression();
      jj_consume_token(18);
      jj_consume_token(14);
{if ("" != null) return a + bcb.customFunc("print");}
      break;
      }
    case 19:{
      jj_consume_token(19);
      a = stmtLIST();
      jj_consume_token(20);
{if ("" != null) return a;}
      break;
      }
    case IF:{
      jj_consume_token(IF);
      a = condition();
      b = statement();
      c = optElse();
{if ("" != null) return bcb.buildIf(a,b,c);}
      break;
      }
    case WHILE:{
      jj_consume_token(WHILE);
      a = condition();
      b = statement();
{if ("" != null) return bcb.buildWhile(a,b);}
      break;
      }
    default:
      jj_la1[5] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    throw new Error("Missing return statement in function");
  }

  static final public String condition() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {String a;
    String b;
    Token t;
    a = expression();
    t = jj_consume_token(COMPOP);
    b = expression();
{if ("" != null) return a + b + bcb.getMnemonic(t.image);}
    throw new Error("Missing return statement in function");
  }

  static final public String optElse() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {String a = "";
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case ELSE:{
      jj_consume_token(ELSE);
      a = statement();
      break;
      }
    default:
      jj_la1[6] = jj_gen;
      ;
    }
{if ("" != null) return a;}
    throw new Error("Missing return statement in function");
  }

  static final public String stmtLIST() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {String a = "";
    String b = "";
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case PRINT:
    case IF:
    case WHILE:
    case IDENT:
    case 19:{
      a = statement();
      b = stmtLIST();
      break;
      }
    default:
      jj_la1[7] = jj_gen;
      ;
    }
{if ("" != null) return a+b;}
    throw new Error("Missing return statement in function");
  }

  static final public String expression() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {String a;
    String b;
    a = term();
    b = summe();
{if ("" != null) return a + b;}
    throw new Error("Missing return statement in function");
  }

  static final public String summe() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {String a = "";
    String b = "";
    String c = "";
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 21:
    case 22:{
      switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
      case 21:{
        jj_consume_token(21);
        a = term();
        b = summe();
c +=  a + b + bcb.iadd();
        break;
        }
      case 22:{
        jj_consume_token(22);
        a = term();
        b = summe();
c += a + b + bcb.isub();
        break;
        }
      default:
        jj_la1[8] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
      break;
      }
    default:
      jj_la1[9] = jj_gen;
      ;
    }
{if ("" != null) return c;}
    throw new Error("Missing return statement in function");
  }

  static final public String term() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {String a;
    String b;
    a = faktor();
    b = produkt();
{if ("" != null) return a + b;}
    throw new Error("Missing return statement in function");
  }

  static final public String produkt() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {String a = "";
    String b = "";
    String c = "";
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 23:
    case 24:{
      switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
      case 23:{
        jj_consume_token(23);
        a = term();
        b = summe();
c += a + bcb.imul() + b;
        break;
        }
      case 24:{
        jj_consume_token(24);
        a = term();
        b = summe();
c += a + bcb.idiv() + b;
        break;
        }
      default:
        jj_la1[10] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
      break;
      }
    default:
      jj_la1[11] = jj_gen;
      ;
    }
{if ("" != null) return c;}
    throw new Error("Missing return statement in function");
  }

  static final public String faktor() throws ParseException, SymbolAlreadyDefinedException, UnknownSymbolException {Token t;
    String s;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case NUMBER:{
      t = jj_consume_token(NUMBER);
{if ("" != null) return bcb.bipush(st.ToHex(Integer.parseInt(t.image)));}
      break;
      }
    case IDENT:{
      t = jj_consume_token(IDENT);
{if ("" != null) return st.GetSymbol(t.image);}
      break;
      }
    case 17:{
      jj_consume_token(17);
      s = expression();
      jj_consume_token(18);
{if ("" != null) return s;}
      break;
      }
    default:
      jj_la1[12] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
    throw new Error("Missing return statement in function");
  }

  static private boolean jj_initialized_once = false;
  /** Generated Token Manager. */
  static public miniParserParserTokenManager token_source;
  static SimpleCharStream jj_input_stream;
  /** Current token. */
  static public Token token;
  /** Next token. */
  static public Token jj_nt;
  static private int jj_ntk;
  static private int jj_gen;
  static final private int[] jj_la1 = new int[13];
  static private int[] jj_la1_0;
  static {
      jj_la1_init_0();
   }
   private static void jj_la1_init_0() {
      jj_la1_0 = new int[] {0x100,0x10000,0x80,0x8000,0x10000,0x82e00,0x1000,0x82e00,0x600000,0x600000,0x1800000,0x1800000,0x22020,};
   }

  /** Constructor with InputStream. */
  public miniParserParser(java.io.InputStream stream) {
     this(stream, null);
  }
  /** Constructor with InputStream and supplied encoding */
  public miniParserParser(java.io.InputStream stream, String encoding) {
    if (jj_initialized_once) {
      System.out.println("ERROR: Second call to constructor of static parser.  ");
      System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
      System.out.println("       during parser generation.");
      throw new Error();
    }
    jj_initialized_once = true;
    try { jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source = new miniParserParserTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 13; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  static public void ReInit(java.io.InputStream stream) {
     ReInit(stream, null);
  }
  /** Reinitialise. */
  static public void ReInit(java.io.InputStream stream, String encoding) {
    try { jj_input_stream.ReInit(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 13; i++) jj_la1[i] = -1;
  }

  /** Constructor. */
  public miniParserParser(java.io.Reader stream) {
    if (jj_initialized_once) {
      System.out.println("ERROR: Second call to constructor of static parser. ");
      System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
      System.out.println("       during parser generation.");
      throw new Error();
    }
    jj_initialized_once = true;
    jj_input_stream = new SimpleCharStream(stream, 1, 1);
    token_source = new miniParserParserTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 13; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  static public void ReInit(java.io.Reader stream) {
    jj_input_stream.ReInit(stream, 1, 1);
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 13; i++) jj_la1[i] = -1;
  }

  /** Constructor with generated Token Manager. */
  public miniParserParser(miniParserParserTokenManager tm) {
    if (jj_initialized_once) {
      System.out.println("ERROR: Second call to constructor of static parser. ");
      System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
      System.out.println("       during parser generation.");
      throw new Error();
    }
    jj_initialized_once = true;
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 13; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(miniParserParserTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 13; i++) jj_la1[i] = -1;
  }

  static private Token jj_consume_token(int kind) throws ParseException {
    Token oldToken;
    if ((oldToken = token).next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    if (token.kind == kind) {
      jj_gen++;
      return token;
    }
    token = oldToken;
    jj_kind = kind;
    throw generateParseException();
  }


/** Get the next Token. */
  static final public Token getNextToken() {
    if (token.next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    jj_gen++;
    return token;
  }

/** Get the specific Token. */
  static final public Token getToken(int index) {
    Token t = token;
    for (int i = 0; i < index; i++) {
      if (t.next != null) t = t.next;
      else t = t.next = token_source.getNextToken();
    }
    return t;
  }

  static private int jj_ntk_f() {
    if ((jj_nt=token.next) == null)
      return (jj_ntk = (token.next=token_source.getNextToken()).kind);
    else
      return (jj_ntk = jj_nt.kind);
  }

  static private java.util.List<int[]> jj_expentries = new java.util.ArrayList<int[]>();
  static private int[] jj_expentry;
  static private int jj_kind = -1;

  /** Generate ParseException. */
  static public ParseException generateParseException() {
    jj_expentries.clear();
    boolean[] la1tokens = new boolean[25];
    if (jj_kind >= 0) {
      la1tokens[jj_kind] = true;
      jj_kind = -1;
    }
    for (int i = 0; i < 13; i++) {
      if (jj_la1[i] == jj_gen) {
        for (int j = 0; j < 32; j++) {
          if ((jj_la1_0[i] & (1<<j)) != 0) {
            la1tokens[j] = true;
          }
        }
      }
    }
    for (int i = 0; i < 25; i++) {
      if (la1tokens[i]) {
        jj_expentry = new int[1];
        jj_expentry[0] = i;
        jj_expentries.add(jj_expentry);
      }
    }
    int[][] exptokseq = new int[jj_expentries.size()][];
    for (int i = 0; i < jj_expentries.size(); i++) {
      exptokseq[i] = jj_expentries.get(i);
    }
    return new ParseException(token, exptokseq, tokenImage);
  }

  /** Enable tracing. */
  static final public void enable_tracing() {
  }

  /** Disable tracing. */
  static final public void disable_tracing() {
  }

    }
