public class ByteCodeBuilder {
    
    String bipush = "10";
    String iload = "15";
    String istore = "36";
    String iadd = "60";
    String isub = "64";
    String imul = "68";
    String idiv = "6c";
    String gleich = "a0";
    String ungleich = "9f"; 
    String groesser = "a3";
    String kleiner = "a1";
    String groesserGleich = "a2";
    String kleinerGleich = "a4";
    String goTo = "a7";
    String reTurn = "b1";
    String customFunc = "b8";

    int registerCounter = 1;
    int[] offsets;

    public String bipush (String value) {
        return bipush + " " + value + " ";
    }
    public String iload (String index) {
        return iload + " " + index + " ";
    }
    public String istore (String index) {
        return istore + " " + index + " ";
    }
    public String iadd () {
        return iadd + " ";
    }
    public String isub () {
        return isub + " ";
    }
    public String imul () {
        return imul + " ";
    }
    public String idiv () {
        return idiv + " ";
    }
    public String reTurn() {
        return reTurn;
    }
    public String customFunc(String functionName) {
        return customFunc + " (" + functionName + ") ";
    }

    public String getMnemonic(String compop) {
        String s = "";
        switch(compop){
			case "==":
				s = gleich + " ";
				break;
		    case "!=":
				s = "9f ";
				break;
		    case ">=":
				s = "a1 ";
				break;
		    case "<=":
				s = "a3 ";
				break;
		    case "<":
				s = "a2 ";
				break;
		    case ">":
				s = "a4 ";
				break;
		}
        return s;
    }

    public String buildIf(String condition, String statement, String optElse) {

        int x = 0;
        int elseOpt = 9;

        if (optElse != "") {
            x = x + elseOpt;
        }

        String withoutMethods = findMethods(statement);
        if (withoutMethods.length() != statement.length()) {
            int count = (withoutMethods.length() - withoutMethods.replace("()", "").length())/2;
            x = x + count*3;
        }

        String returninger = condition+ toHex((9 /* compop von condition */ + x + withoutMethods.length())/3, true) + statement;

        if (optElse != "") {
            x = 9;
            withoutMethods = findMethods(optElse);
            if (withoutMethods.length() != optElse.length()) {
                int count = (withoutMethods.length() - withoutMethods.replace("()", "").length())/2;
                x = x + count*3;
            }

            returninger += goTo + " " + toHex((withoutMethods.length()+ x)/3, true) + optElse;
        }

        return returninger;
    }

    public String buildWhile(String condition, String statement) {
        int x = 0;
        String withoutMethods = findMethods(statement);
        if (withoutMethods.length() != statement.length()) {
                int count = (withoutMethods.length() - withoutMethods.replace("()", "").length())/2;
                x = x + (3*count);
        }
        return condition + toHex((9 /* compop von condition */ + 9 /* goto */ + x + withoutMethods.length())/3, true) + statement + goTo + " " + toHex(255 - ((withoutMethods.length()+x)/3)-6, false);
    }

    private String findMethods(String statement) { 
        return statement.replaceAll("\\(.+?\\)","()");
    }

    private String toHex(int offset, boolean positive) {
        String hex = Integer.toHexString(offset);
        if (positive) {
            if (hex.length() == 1) {
                hex = "00 0" + hex + " ";
            } else if (hex.length() == 2) {
                hex = "00 " + hex + " ";
            } else if (hex.length() == 3) {
                hex= "0" + hex.charAt(0) + " " + hex.charAt(1) + hex.charAt(2) + " ";
            }
        } else {
            if (hex.length() == 1) {
                hex = "ff f" + hex + " ";
            } else if (hex.length() == 2) {
                hex = "ff " + hex + " ";
            } else if (hex.length() == 3) {
                hex= "f" + hex.charAt(0) + " " + hex.charAt(1) + hex.charAt(2) + " ";
            }
        }
        return hex;
    }
}