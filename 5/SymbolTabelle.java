import java.util.HashMap;

public class SymbolTabelle {

    public class ValueAdress{
        String value;
        String adress;
    }
        
    HashMap<String,ValueAdress> symbole = new HashMap<String,ValueAdress>();

    private static ByteCodeBuilder bcb = new ByteCodeBuilder();

    int counter = 1;

    public String ToHex(int value) {
        String hex = Integer.toHexString(value);
        if (hex.length() == 1) {
            hex = "0" + hex;
        }
        return hex;
    }
    
    public void AddKonstante(String key, String value) throws SymbolAlreadyDefinedException {
        if (symbole.containsKey(key)){
            throw new SymbolAlreadyDefinedException( key + " bereits deklariert!");
        } 
        ValueAdress va = new ValueAdress();
        va.value = ToHex(Integer.parseInt(value));
        va.adress = "";
        symbole.put(key, va);
    }

    public String AddVariable(String key, int value) throws SymbolAlreadyDefinedException {
        if (symbole.containsKey(key)){
            throw new SymbolAlreadyDefinedException( key + " bereits deklariert!");
        } 
        ValueAdress va = new ValueAdress();
        va.value = ToHex(value);
        va.adress = ToHex(counter);
        symbole.put(key,va);
        counter ++;
        return  bcb.bipush(va.value) + bcb.istore(va.adress);
    }

    public String GetAdress(String key)  throws UnknownSymbolException {
		if(!symbole.containsKey(key)){
			throw new UnknownSymbolException(key + " existiert nicht!");
		}
        return symbole.get(key).adress;
    }

    public String GetSymbol(String key) throws UnknownSymbolException {
		if(!symbole.containsKey(key)){
			throw new UnknownSymbolException(key + " existiert nicht!");
		}
        ValueAdress x = symbole.get(key);
        if (x.adress == "") {
            return bcb.bipush(x.value);
        } else {
            return bcb.iload(x.adress);
        }
    }

}