/* MiniJavaFunProc.java */
/* Generated By:JavaCC: Do not edit this line. MiniJavaFunProc.java */
        import p3.*;
        import jcfg.*;
        import java.util.HashMap;
        import java.util.Map;
        import java.util.ArrayList;

        public class MiniJavaFunProc implements MiniJavaFunProcConstants {
                static Boolean lesbar = false;
                static Map<String, Method> methodList = new HashMap<String, Method>();

                enum ByteCode {
                        BIPUSH ("10", " bipush "),
                        SIPUSH ("11", " sipush "),
                        ILOAD ("15", " iload "),
                        ISTORE ("36", " istore "),
                        IADD ("60", " iadd "),
                        ISUB ("64", " isub "),
                        IMUL ("68", " imul "),
                        IDIV ("6c", " idiv "),
                        IF_ICMPEQ ("9f", " if_cmpeq "),
                        IF_ICMPNE ("a0", " if_cmpne "),
                        IF_ICMPLT ("a1", " if_cmplt "),
                        IF_ICMPGE ("a2", " if_cmpge "),
                        IF_ICMPGT ("a3", " if_cmpgt "),
                        IF_ICMPLE ("a4", " if_cmple "),
                        GETSTATIC ("b2", " getstatic "),
                        PUTSTATIC ("b3", " putstatic "),
                        GOTO ("a7", " goto "),
                        IRETURN ("ac", " ireturn "),
                        RETURN ("b1", " return "),
                        INVOKESTATIC ("b8", " invokestatic ");

                        public final String code;

                        private ByteCode(String optCode, String mnemonic){
                                if(lesbar)
                                        code = mnemonic;
                                else
                                        code = optCode;
                        }
                }

                public static void main(String args[])
                {
                        if(args.length > 0) {
                                lesbar = true;
                        }

                        MiniJavaFunProc parser = new MiniJavaFunProc(System.in);

                        try {
                                parser.program();

                                ArrayList<MethodObject> methods = new ArrayList<MethodObject>();
                                for(Map.Entry<String, Method> entry : methodList.entrySet()){
                                        Object obj = entry.getValue();
                                        if(obj instanceof Method)
                                        {
                                                Method meh = (Method)obj;
                                                System.out.println("Function: ---- " + meh.getName());
                                                System.out.println(meh.getCode());
                                                methods.add(meh.getMethodObject());
                                        }
                                }

                                JavaClassFileGenerator j = new JavaClassFileGenerator("Test", true, false, false);
                                j.generateClassFile(methods.toArray(new MethodObject[0]));
                        } catch (ParseException e) {
                                System.err.println(e);
                        } catch(UnknownSymbolException e) {
                                System.err.println(e);
                        } catch(SymbolAlreadyDefinedException e) {
                                System.err.println(e);
                        } catch(LWertException e) {
                                System.err.println(e);
                        } catch(RWertException e) {
                                System.err.println(e);
                        } catch(WrongParametersException e) {
                                System.err.println(e);
                        }
                }

                public static String toHex(String num){
                        return String.format("%02x",Integer.parseInt(num));
                }
                public static String toJmpCount(int toJmpOver){
                        String ret = String.format("%04x",toJmpOver);
                        return ret.substring(ret.length() - 4);
                }
                public static void addFunProcCall(Method method, String id){
                        addKombiBefehl(method, ByteCode.INVOKESTATIC.code, "(" + id + ")", 3);
                }

                public static void addReturn(Method method){
                        method.addCode(ByteCode.RETURN.code);
                        method.addByteCount(1);
                }

                public static void addKombiBefehl(Method method, String eins, String zwei, int bytes){
                        method.addCode(eins);
                        method.addCode(zwei);
                        method.addByteCount(bytes);
                }

  final public void program() throws ParseException, UnknownSymbolException, SymbolAlreadyDefinedException, LWertException, RWertException, WrongParametersException {Method main;
main = new Method("main", 1);
                constDecl(main); varDecl(main);
                methodList.put("main", main);
                procedure(); function(); statement(main); addReturn(main);
                methodList.put("main", main);
    jj_consume_token(0);
  }

  final public void constDecl(Method method) throws ParseException, SymbolAlreadyDefinedException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case CONSTDECL:{
      jj_consume_token(CONSTDECL);
      constZuw(method);
      constList(method);
      jj_consume_token(SEMICOLON);
      break;
      }
    default:
      jj_la1[0] = jj_gen;

    }
  }

  final public void constZuw(Method method) throws ParseException, SymbolAlreadyDefinedException {String id, num;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case IDENT:{
      jj_consume_token(IDENT);
id=token.image;
      jj_consume_token(EQUALS);
      jj_consume_token(NUM);
num=token.image;
                method.addConstant(id, num);
      break;
      }
    default:
      jj_la1[1] = jj_gen;

    }
  }

  final public void constList(Method method) throws ParseException, SymbolAlreadyDefinedException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case COMMA:{
      jj_consume_token(COMMA);
      constZuw(method);
      constList(method);
      break;
      }
    default:
      jj_la1[2] = jj_gen;

    }
  }

  final public void varDecl(Method method) throws ParseException, SymbolAlreadyDefinedException {String ident;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case INT:{
      jj_consume_token(INT);
      jj_consume_token(IDENT);
ident = token.image;
      varZuw(method.addVariable(ident), method);
if(method.isMain())
                {
                        addKombiBefehl(method, ByteCode.PUTSTATIC.code, "["+ ident + "]", 3);
                }
      varList(method);
      jj_consume_token(SEMICOLON);
      break;
      }
    default:
      jj_la1[3] = jj_gen;

    }
  }

  final public void varZuw(String adress, Method method) throws ParseException, SymbolAlreadyDefinedException {String num = "0", eins, zwei, bytes;
        Tuple<Boolean, String> tup;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case EQUALS:{
      jj_consume_token(EQUALS);
      jj_consume_token(NUM);
num = token.image;
      break;
      }
    default:
      jj_la1[4] = jj_gen;

    }
addKombiBefehl(method, ByteCode.BIPUSH.code, toHex(num), 2);
                if(!method.isMain())
                {
                        addKombiBefehl(method, ByteCode.ISTORE.code, toHex(adress), 2);
                }
  }

  final public void varList(Method method) throws ParseException, SymbolAlreadyDefinedException {String ident;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case COMMA:{
      jj_consume_token(COMMA);
      jj_consume_token(IDENT);
ident = token.image;
                varZuw(method.addVariable(ident), method);

                if(method.isMain())
                {
                        addKombiBefehl(method, ByteCode.PUTSTATIC.code, "["+ ident + "]", 3);
                }
      varList(method);
      break;
      }
    default:
      jj_la1[5] = jj_gen;

    }
  }

  final public void procedure() throws ParseException, UnknownSymbolException, SymbolAlreadyDefinedException, LWertException, RWertException, WrongParametersException {Method proc;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case VOID:{
      jj_consume_token(VOID);
      jj_consume_token(IDENT);
proc = new Method(token.image, false);
                params(proc);
      jj_consume_token(CBRACKETAUF);
      routine(proc);
      jj_consume_token(CBRACKETZU);
addReturn(proc);
                methodList.put(proc.getName(), proc);
      procedure();
      break;
      }
    default:
      jj_la1[6] = jj_gen;

    }
  }

  final public void function() throws ParseException, UnknownSymbolException, SymbolAlreadyDefinedException, LWertException, RWertException, WrongParametersException {Method func;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case FUNC:{
      jj_consume_token(FUNC);
      jj_consume_token(IDENT);
func = new Method(token.image, true);
                params(func);
      jj_consume_token(CBRACKETAUF);
      routine(func);
      jj_consume_token(RETURN);
      expression(func);
      jj_consume_token(SEMICOLON);
      jj_consume_token(CBRACKETZU);
func.addCode(ByteCode.IRETURN.code);
                func.addByteCount(1);
                methodList.put(func.getName(), func);
      function();
      break;
      }
    default:
      jj_la1[7] = jj_gen;

    }
  }

  final public void params(Method method) throws ParseException, SymbolAlreadyDefinedException {
    jj_consume_token(BRACKETAUF);
    param(method);
    jj_consume_token(BRACKETZU);
  }

  final public void param(Method method) throws ParseException, SymbolAlreadyDefinedException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case INT:{
      jj_consume_token(INT);
      jj_consume_token(IDENT);
method.addVariable(token.image);
                method.addParam();
      optParams(method);
      break;
      }
    default:
      jj_la1[8] = jj_gen;

    }
  }

  final public void optParams(Method method) throws ParseException, SymbolAlreadyDefinedException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case COMMA:{
      jj_consume_token(COMMA);
      jj_consume_token(INT);
      jj_consume_token(IDENT);
method.addVariable(token.image);
                method.addParam();
      optParams(method);
      break;
      }
    default:
      jj_la1[9] = jj_gen;

    }
  }

  final public void routine(Method method) throws ParseException, UnknownSymbolException, SymbolAlreadyDefinedException, LWertException, WrongParametersException, RWertException {
    constDecl(method);
    varDecl(method);
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case IF:
    case WHILE:
    case PRINT:
    case CBRACKETAUF:
    case IDENT:{
      statement(method);
      break;
      }
    default:
      jj_la1[10] = jj_gen;

    }
  }

  final public void expression(Method method) throws ParseException, UnknownSymbolException, WrongParametersException, RWertException {
    term(method);
    summe(method);
  }

  final public void summe(Method method) throws ParseException, UnknownSymbolException, WrongParametersException, RWertException {String opRead, opWrite;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case PM:{
      jj_consume_token(PM);
opRead = token.image;
      term(method);
if(opRead.equals("+"))
                        opWrite = ByteCode.IADD.code;
                else
                        opWrite = ByteCode.ISUB.code;
                method.addCode(opWrite);
                method.addByteCount(1);
      summe(method);
      break;
      }
    default:
      jj_la1[11] = jj_gen;

    }
  }

  final public void term(Method method) throws ParseException, UnknownSymbolException, WrongParametersException, RWertException {
    faktor(method);
    produkt(method);
  }

  final public void produkt(Method method) throws ParseException, UnknownSymbolException, WrongParametersException, RWertException {String opRead, opWrite;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case MG:{
      jj_consume_token(MG);
opRead = token.image;
      faktor(method);
if(opRead.equals("*"))
                        opWrite = ByteCode.IMUL.code;
                else
                        opWrite = ByteCode.IDIV.code;
                method.addCode(opWrite);
                method.addByteCount(1);
      produkt(method);
      break;
      }
    default:
      jj_la1[12] = jj_gen;

    }
  }

  final public void faktor(Method method) throws ParseException, UnknownSymbolException, WrongParametersException, RWertException {String id;
        String num;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case NUM:{
      jj_consume_token(NUM);
num = token.image;
                addKombiBefehl(method, ByteCode.BIPUSH.code, toHex(num), 2);
      break;
      }
    case IDENT:{
      jj_consume_token(IDENT);
id = token.image;
      ident(method, id);
      break;
      }
    case BRACKETAUF:{
      jj_consume_token(BRACKETAUF);
      expression(method);
      jj_consume_token(BRACKETZU);
      break;
      }
    default:
      jj_la1[13] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
  }

  final public void ident(Method method, String id) throws ParseException, UnknownSymbolException, WrongParametersException, RWertException {int funcParams;
        Boolean helpMain;
        Tuple <Boolean, String> wert;
        Method calledFunc;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case BRACKETAUF:{
      jj_consume_token(BRACKETAUF);
calledFunc = methodList.get(id);
                if(calledFunc == null){
                        {if (true) throw new UnknownSymbolException(id);}
                }
                if(!calledFunc.hasReturnValue())
                {
                        {if (true) throw new RWertException(id);}
                }
      functionCall(method, id, calledFunc.getParamCount());
      jj_consume_token(BRACKETZU);
addFunProcCall(method, id);
      break;
      }
    default:
      jj_la1[14] = jj_gen;
try{
                        helpMain = method.isMain();
                        wert = method.getSymbol(id);
                }catch(UnknownSymbolException e){
                        Method m = methodList.get("main");
                        wert = m.getSymbol(id);
                        helpMain = true;
                }
                if(!wert.x){
                        addKombiBefehl(method, ByteCode.BIPUSH.code, toHex(wert.y), 2);
                }
                else if(helpMain){
                        addKombiBefehl(method, ByteCode.GETSTATIC.code, "[" + id + "]", 3);
                }
                else{
                        addKombiBefehl(method, ByteCode.ILOAD.code, toHex(wert.y), 2);
                }
    }
  }

  final public void functionCall(Method method, String calledFunc, int expectedParamCount) throws ParseException, UnknownSymbolException, WrongParametersException, RWertException {int pCounter = 1;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case NUM:
    case BRACKETAUF:
    case IDENT:{
      expression(method);
pCounter += optExpression(method);
                if (pCounter != expectedParamCount)
                {
                        {if (true) throw new WrongParametersException(calledFunc);}
                }
      break;
      }
    default:
      jj_la1[15] = jj_gen;

    }
  }

  final public void procCall(Method method, String calledProc, int expectedParamCount) throws ParseException, UnknownSymbolException, WrongParametersException, RWertException {int pCounter = 1;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case NUM:
    case BRACKETAUF:
    case IDENT:{
      expression(method);
pCounter += optExpression(method);
                if(pCounter != expectedParamCount)
                {
                        {if (true) throw new WrongParametersException(calledProc);}
                }
      break;
      }
    default:
      jj_la1[16] = jj_gen;

    }
  }

  final public int optExpression(Method method) throws ParseException, UnknownSymbolException, RWertException, WrongParametersException {int pCounter = 0;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case COMMA:{
      jj_consume_token(COMMA);
      expression(method);
pCounter += optExpression(method);
                {if ("" != null) return (pCounter+1);}
      break;
      }
    default:
      jj_la1[17] = jj_gen;
{if ("" != null) return pCounter;}
    }
    throw new Error("Missing return statement in function");
  }

  final public void condition(Method method) throws ParseException, UnknownSymbolException, RWertException, WrongParametersException {String op = "";
    expression(method);
    jj_consume_token(COMPOP);
op = token.image;
    expression(method);
switch (op) {
                        case "==":
                                method.addCode(ByteCode.IF_ICMPNE.code);
                                break;
                        case "<":
                                method.addCode(ByteCode.IF_ICMPGE.code);
                                break;
                        case ">":
                                method.addCode(ByteCode.IF_ICMPLE.code);
                                break;
                        case "!=":
                                method.addCode(ByteCode.IF_ICMPEQ.code);
                                break;
                        case "<=":
                                method.addCode(ByteCode.IF_ICMPGT.code);
                                break;
                        case ">=":
                                method.addCode(ByteCode.IF_ICMPLT.code);
                                break;
                        default:
                                System.out.println("COMPOP nicht lesbar");
                }
                method.addByteCount(1);
  }

  final public void statement(Method method) throws ParseException, UnknownSymbolException, LWertException, RWertException, WrongParametersException {int startStmt, endStmt, startChar, ifOffset = 3, startCond, whileOffset = 6;
        String id = "";
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case IDENT:{
      jj_consume_token(IDENT);
id = token.image;
                idCall(method, id);
      break;
      }
    case PRINT:{
      jj_consume_token(PRINT);
      jj_consume_token(BRACKETAUF);
      expression(method);
addFunProcCall(method, "print");
      jj_consume_token(BRACKETZU);
      jj_consume_token(SEMICOLON);
      break;
      }
    case CBRACKETAUF:{
      jj_consume_token(CBRACKETAUF);
      stmtLIST(method);
      jj_consume_token(CBRACKETZU);
      break;
      }
    case IF:{
      jj_consume_token(IF);
      condition(method);
startStmt = method.getByteCount();
                startChar = method.codeLength();
                statement(method);
                endStmt = method.getByteCount();
                optElse(method);
                if(endStmt != method.getByteCount())
                        ifOffset += 3;
                method.insertCode(startChar ,toJmpCount((endStmt - startStmt) + ifOffset));
                method.addByteCount(2);
      break;
      }
    case WHILE:{
      jj_consume_token(WHILE);
startCond = method.getByteCount();
                condition(method);
                startStmt = method.getByteCount();
                startChar = method.codeLength();
                statement(method);
                method.insertCode(startChar, toJmpCount((method.getByteCount() - startStmt) + whileOffset));
                method.addByteCount(2);
                addKombiBefehl(method, ByteCode.GOTO.code, toJmpCount(startCond - method.getByteCount()), 3);
      break;
      }
    default:
      jj_la1[18] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
  }

  final public void idCall(Method method, String id) throws ParseException, UnknownSymbolException, LWertException, RWertException, WrongParametersException {Tuple<Boolean, String> tup;
        Boolean helpMain;
        Method calledProc;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case BRACKETAUF:{
      jj_consume_token(BRACKETAUF);
calledProc = methodList.get(id);
                procCall(method, id, calledProc.getParamCount());
      jj_consume_token(BRACKETZU);
      jj_consume_token(SEMICOLON);
addFunProcCall(method, id);
      break;
      }
    case EQUALS:{
      jj_consume_token(EQUALS);
      expression(method);
      jj_consume_token(SEMICOLON);
helpMain = method.isMain();
                try{
                        tup = method.getSymbol(id);
                } catch(UnknownSymbolException e){
                        Method m = methodList.get("main");
                        tup = m.getSymbol(id);
                        helpMain = true;
                }
                if(tup.x)
                {
                        if(helpMain)
                        {
                                addKombiBefehl(method, ByteCode.PUTSTATIC.code, "[" + id + "]", 3);
                        }else  {
                                addKombiBefehl(method, ByteCode.ISTORE.code, toHex(tup.y), 2);
                        }
                }else {
                        {if (true) throw new LWertException(id);}
                }
      break;
      }
    default:
      jj_la1[19] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
  }

  final public void optElse(Method method) throws ParseException, UnknownSymbolException, LWertException, RWertException, WrongParametersException {int startByte, endByte, startChar, gotoOffset = 3;
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case ELSE:{
      jj_consume_token(ELSE);
method.addCode(ByteCode.GOTO.code);
                method.addByteCount(1);

                startByte = method.getByteCount();
                startChar = method.codeLength();
                statement(method);
                endByte = method.getByteCount();
                method.insertCode(startChar, toJmpCount((endByte - startByte) + gotoOffset));
                method.addByteCount(2);
      break;
      }
    default:
      jj_la1[20] = jj_gen;

    }
  }

  final public void stmtLIST(Method method) throws ParseException, UnknownSymbolException, LWertException, RWertException, WrongParametersException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case IF:
    case WHILE:
    case PRINT:
    case CBRACKETAUF:
    case IDENT:{
      statement(method);
      stmtLIST(method);
      break;
      }
    default:
      jj_la1[21] = jj_gen;

    }
  }

  /** Generated Token Manager. */
  public MiniJavaFunProcTokenManager token_source;
  SimpleCharStream jj_input_stream;
  /** Current token. */
  public Token token;
  /** Next token. */
  public Token jj_nt;
  private int jj_ntk;
  private int jj_gen;
  final private int[] jj_la1 = new int[22];
  static private int[] jj_la1_0;
  static {
      jj_la1_init_0();
   }
   private static void jj_la1_init_0() {
      jj_la1_0 = new int[] {0x100,0x1000000,0x8000,0x2000,0x4000,0x8000,0x200000,0x400000,0x2000,0x8000,0x1081a00,0x40,0x80,0x1020020,0x20000,0x1020020,0x1020020,0x8000,0x1081a00,0x24000,0x400,0x1081a00,};
   }

  /** Constructor with InputStream. */
  public MiniJavaFunProc(java.io.InputStream stream) {
     this(stream, null);
  }
  /** Constructor with InputStream and supplied encoding */
  public MiniJavaFunProc(java.io.InputStream stream, String encoding) {
    try { jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source = new MiniJavaFunProcTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 22; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(java.io.InputStream stream) {
     ReInit(stream, null);
  }
  /** Reinitialise. */
  public void ReInit(java.io.InputStream stream, String encoding) {
    try { jj_input_stream.ReInit(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 22; i++) jj_la1[i] = -1;
  }

  /** Constructor. */
  public MiniJavaFunProc(java.io.Reader stream) {
    jj_input_stream = new SimpleCharStream(stream, 1, 1);
    token_source = new MiniJavaFunProcTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 22; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(java.io.Reader stream) {
    jj_input_stream.ReInit(stream, 1, 1);
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 22; i++) jj_la1[i] = -1;
  }

  /** Constructor with generated Token Manager. */
  public MiniJavaFunProc(MiniJavaFunProcTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 22; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(MiniJavaFunProcTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 22; i++) jj_la1[i] = -1;
  }

  private Token jj_consume_token(int kind) throws ParseException {
    Token oldToken;
    if ((oldToken = token).next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    if (token.kind == kind) {
      jj_gen++;
      return token;
    }
    token = oldToken;
    jj_kind = kind;
    throw generateParseException();
  }


/** Get the next Token. */
  final public Token getNextToken() {
    if (token.next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    jj_gen++;
    return token;
  }

/** Get the specific Token. */
  final public Token getToken(int index) {
    Token t = token;
    for (int i = 0; i < index; i++) {
      if (t.next != null) t = t.next;
      else t = t.next = token_source.getNextToken();
    }
    return t;
  }

  private int jj_ntk_f() {
    if ((jj_nt=token.next) == null)
      return (jj_ntk = (token.next=token_source.getNextToken()).kind);
    else
      return (jj_ntk = jj_nt.kind);
  }

  private java.util.List<int[]> jj_expentries = new java.util.ArrayList<int[]>();
  private int[] jj_expentry;
  private int jj_kind = -1;

  /** Generate ParseException. */
  public ParseException generateParseException() {
    jj_expentries.clear();
    boolean[] la1tokens = new boolean[26];
    if (jj_kind >= 0) {
      la1tokens[jj_kind] = true;
      jj_kind = -1;
    }
    for (int i = 0; i < 22; i++) {
      if (jj_la1[i] == jj_gen) {
        for (int j = 0; j < 32; j++) {
          if ((jj_la1_0[i] & (1<<j)) != 0) {
            la1tokens[j] = true;
          }
        }
      }
    }
    for (int i = 0; i < 26; i++) {
      if (la1tokens[i]) {
        jj_expentry = new int[1];
        jj_expentry[0] = i;
        jj_expentries.add(jj_expentry);
      }
    }
    int[][] exptokseq = new int[jj_expentries.size()][];
    for (int i = 0; i < jj_expentries.size(); i++) {
      exptokseq[i] = jj_expentries.get(i);
    }
    return new ParseException(token, exptokseq, tokenImage);
  }

  /** Enable tracing. */
  final public void enable_tracing() {
  }

  /** Disable tracing. */
  final public void disable_tracing() {
  }

        }
