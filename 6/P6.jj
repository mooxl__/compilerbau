options
{
	static = false;
}
PARSER_BEGIN(MiniJavaFunProc)
	import p3.*;	
	import jcfg.*;
	import java.util.HashMap;
	import java.util.Map;
	import java.util.ArrayList;
	
	public class MiniJavaFunProc
	{
		static Boolean lesbar = false;
		static Map<String, Method> methodList = new HashMap<String, Method>();

		enum ByteCode {
			BIPUSH ("10", " bipush "),
			SIPUSH ("11", " sipush "),
			ILOAD ("15", " iload "),
			ISTORE ("36", " istore "),
			IADD ("60", " iadd "),
			ISUB ("64", " isub "),
			IMUL ("68", " imul "),
			IDIV ("6c", " idiv "),
			IF_ICMPEQ ("9f", " if_cmpeq "),
			IF_ICMPNE ("a0", " if_cmpne "),
			IF_ICMPLT ("a1", " if_cmplt "),
			IF_ICMPGE ("a2", " if_cmpge "),
			IF_ICMPGT ("a3", " if_cmpgt "),
			IF_ICMPLE ("a4", " if_cmple "),
			GETSTATIC ("b2", " getstatic "),
			PUTSTATIC ("b3", " putstatic "),
			GOTO ("a7", " goto "),
			IRETURN ("ac", " ireturn "),
			RETURN ("b1", " return "),
			INVOKESTATIC ("b8", " invokestatic ");
			
			public final String code;
			
			private ByteCode(String optCode, String mnemonic){
				if(lesbar)
					code = mnemonic;
				else 
					code = optCode;
			}
		}

		public static void main(String args[]) 
		{
			if(args.length > 0) {
				lesbar = true;
			}

			MiniJavaFunProc parser = new MiniJavaFunProc(System.in);
			
			try {
				parser.program();

				ArrayList<MethodObject> methods = new ArrayList<MethodObject>();
				for(Map.Entry<String, Method> entry : methodList.entrySet()){
					Object obj = entry.getValue();
					if(obj instanceof Method)
					{
						Method meh = (Method)obj;
						System.out.println("Function: ---- " + meh.getName());
						System.out.println(meh.getCode());
						methods.add(meh.getMethodObject());
					}
				}

				JavaClassFileGenerator j = new JavaClassFileGenerator("Test", true, false, false);
				j.generateClassFile(methods.toArray(new MethodObject[0]));
			} catch (ParseException e) {
				System.err.println(e);
			} catch(UnknownSymbolException e) {
				System.err.println(e);
			} catch(SymbolAlreadyDefinedException e) {
				System.err.println(e);
			} catch(LWertException e) {
				System.err.println(e);
			} catch(RWertException e) {
				System.err.println(e);
			} catch(WrongParametersException e) {
				System.err.println(e);
			}
		}

		public static String toHex(String num){
			return String.format("%02x",Integer.parseInt(num));
		}
		public static String toJmpCount(int toJmpOver){
			String ret = String.format("%04x",toJmpOver);
			return ret.substring(ret.length() - 4);
		}
		public static void addFunProcCall(Method method, String id){
			addKombiBefehl(method, ByteCode.INVOKESTATIC.code, "(" + id + ")", 3);
		}

		public static void addReturn(Method method){
			method.addCode(ByteCode.RETURN.code);
			method.addByteCount(1);
		}

		public static void addKombiBefehl(Method method, String eins, String zwei, int bytes){
			method.addCode(eins);
			method.addCode(zwei);
			method.addByteCount(bytes);
		}
	}
PARSER_END(MiniJavaFunProc)

SKIP:
{
	" "
	| "\r"
	| "\n"
	| "\t"
}

TOKEN:
{
	 < NUM : ["0"]|["1"-"9"](["0"-"9"])* >
	|< PM : ["+","-"] >
	|< MG : ["*","/"] >
	|< CONSTDECL : "final " <INT> >
	|< IF : "if" >
	|< ELSE : "else" >
	|< WHILE : "while" >
	|< PRINT : "print" >
	|< INT : "int" >
	|< EQUALS : "=" >
	|< COMMA : "," >
	|< SEMICOLON : ";" >
	|< BRACKETAUF : "(" >
	|< BRACKETZU : ")" >
	|< CBRACKETAUF : "{" >
	|< CBRACKETZU : "}" >
	|< VOID : "void" >
	|< FUNC : "func" >
	|< RETURN : "return" >
	|< IDENT : ["a"-"z"](["a"-"z"]|["A"-"Z"]|["0"-"9"])* >
	|< COMPOP : ["<", ">"]|"=="|">="|"<="|"!=" >
}

void program() throws UnknownSymbolException, SymbolAlreadyDefinedException, LWertException, RWertException, WrongParametersException :
{
	Method main;
}
{	
	{
		main = new Method("main", 1);
		constDecl(main); varDecl(main);
		methodList.put("main", main);
		procedure(); function(); statement(main); addReturn(main);
		methodList.put("main", main);
	}
	<EOF>
}

void constDecl(Method method) throws SymbolAlreadyDefinedException:
{}
{
	<CONSTDECL> constZuw(method) constList(method) <SEMICOLON> | {}
}

void constZuw(Method method) throws SymbolAlreadyDefinedException:
{
	String id, num;
}
{
	<IDENT> 
	{
		id=token.image;
	} 
	<EQUALS> <NUM> 
	{
		num=token.image; 
		method.addConstant(id, num);
	} 
	| {}
}

void constList(Method method) throws SymbolAlreadyDefinedException:
{}
{
	<COMMA> constZuw(method) constList(method) | {}
}

void varDecl(Method method) throws SymbolAlreadyDefinedException:
{
	String ident;
}
{
	<INT> <IDENT> 
	{ 
		ident = token.image;
	}
	varZuw(method.addVariable(ident), method)
	{
		if(method.isMain())
		{
			addKombiBefehl(method, ByteCode.PUTSTATIC.code, "["+ ident + "]", 3);
		}
	} 
	varList(method) <SEMICOLON> | {}
}

void varZuw(String adress, Method method) throws SymbolAlreadyDefinedException:
{
	String num = "0", eins, zwei, bytes;
	Tuple<Boolean, String> tup;
}
{
	( <EQUALS> <NUM> { num = token.image; } | {} )
	{
		addKombiBefehl(method, ByteCode.BIPUSH.code, toHex(num), 2);
		if(!method.isMain())
		{
			addKombiBefehl(method, ByteCode.ISTORE.code, toHex(adress), 2);
		}
	}
}

void varList(Method method) throws SymbolAlreadyDefinedException:
{
	String ident;
}
{
	<COMMA> <IDENT> 
	{ 
		ident = token.image;
		varZuw(method.addVariable(ident), method);
		
		if(method.isMain())
		{
			addKombiBefehl(method, ByteCode.PUTSTATIC.code, "["+ ident + "]", 3);
		}
	} varList(method) | {}
}

void procedure() throws UnknownSymbolException, SymbolAlreadyDefinedException, LWertException, RWertException, WrongParametersException:
{
	Method proc;
}
{
	<VOID> <IDENT> 
	{
		proc = new Method(token.image, false);
		params(proc); 
	}
	<CBRACKETAUF> routine(proc) <CBRACKETZU>
	{
		addReturn(proc);
		methodList.put(proc.getName(), proc);
	}
	procedure() | {}
}

void function() throws UnknownSymbolException, SymbolAlreadyDefinedException, LWertException, RWertException, WrongParametersException:
{
	Method func;
}
{
	<FUNC> <IDENT>
	{ 
		func = new Method(token.image, true);
		params(func);
	}
	<CBRACKETAUF> routine(func) <RETURN> expression(func) <SEMICOLON> <CBRACKETZU>
	{
		func.addCode(ByteCode.IRETURN.code);
		func.addByteCount(1);
		methodList.put(func.getName(), func);
	} 
	function() | {}
}

void params(Method method) throws SymbolAlreadyDefinedException:
{}
{
	<BRACKETAUF> param(method) <BRACKETZU>
}

void param(Method method) throws SymbolAlreadyDefinedException:
{}
{
	<INT> <IDENT> 
	{ 
		method.addVariable(token.image);
		method.addParam();
	} 
	optParams(method) | {}
}

void optParams(Method method) throws SymbolAlreadyDefinedException:
{}
{
	<COMMA> <INT> <IDENT> 
	{ 
		method.addVariable(token.image);
		method.addParam();
	} 
	optParams(method) | {}
}

void routine(Method method) throws UnknownSymbolException, SymbolAlreadyDefinedException, LWertException, WrongParametersException, RWertException:
{}
{
	constDecl(method) varDecl(method) (statement(method) | {})
}

void expression(Method method) throws UnknownSymbolException, WrongParametersException, RWertException:
{}
{
	term(method) summe(method)	
}

void summe(Method method) throws UnknownSymbolException, WrongParametersException, RWertException:
{
	String opRead, opWrite;
}
{
	<PM> 
	{ 
		opRead = token.image; 
	} 
	term(method)
	{
		if(opRead.equals("+"))
			opWrite = ByteCode.IADD.code;
		else
			opWrite = ByteCode.ISUB.code;
		method.addCode(opWrite);
		method.addByteCount(1);
	} 
	summe(method) | {}
}

void term(Method method) throws UnknownSymbolException, WrongParametersException, RWertException:
{}
{
	faktor(method) produkt(method)
}

void produkt(Method method) throws UnknownSymbolException, WrongParametersException, RWertException:
{
	String opRead, opWrite;
}
{
	<MG> 
	{ 
		opRead = token.image; 
	}
	faktor(method) 
	{
		if(opRead.equals("*"))
			opWrite = ByteCode.IMUL.code;
		else
			opWrite = ByteCode.IDIV.code;
		method.addCode(opWrite);
		method.addByteCount(1);
	}
	produkt(method) | {}
}

void faktor(Method method) throws UnknownSymbolException, WrongParametersException, RWertException:
{
	String id; 
	String num;
}
{
	<NUM>
	{
		num = token.image;
		addKombiBefehl(method, ByteCode.BIPUSH.code, toHex(num), 2);
	}
	| <IDENT> {id = token.image;} ident(method, id) 
	| <BRACKETAUF> expression(method) <BRACKETZU>
}

void ident(Method method, String id) throws UnknownSymbolException, WrongParametersException, RWertException:
{
	int funcParams;
	Boolean helpMain;
	Tuple <Boolean, String> wert;
	Method calledFunc;
}
{

	<BRACKETAUF>
	{
		calledFunc = methodList.get(id);
		if(calledFunc == null){
			throw new UnknownSymbolException(id);
		}
		if(!calledFunc.hasReturnValue())
		{
			throw new RWertException(id);
		}
	}
	functionCall(method, id, calledFunc.getParamCount()) <BRACKETZU> {addFunProcCall(method, id);}
	|
	{
		try{
			helpMain = method.isMain();
			wert = method.getSymbol(id);
		}catch(UnknownSymbolException e){
			Method m = methodList.get("main");
			wert = m.getSymbol(id);
			helpMain = true;
		}
		if(!wert.x){
			addKombiBefehl(method, ByteCode.BIPUSH.code, toHex(wert.y), 2);
		}
		else if(helpMain){
			addKombiBefehl(method, ByteCode.GETSTATIC.code, "[" + id + "]", 3);
		}
		else{
			addKombiBefehl(method, ByteCode.ILOAD.code, toHex(wert.y), 2);
		}
	}
}

void functionCall(Method method, String calledFunc, int expectedParamCount) throws UnknownSymbolException, WrongParametersException, RWertException:
{
	int pCounter = 1;
}
{ 
	expression(method) 
	{
		pCounter += optExpression(method);
		if (pCounter != expectedParamCount)
		{
			throw new WrongParametersException(calledFunc);
		}
	}
	|{}
}

void procCall(Method method, String calledProc, int expectedParamCount) throws UnknownSymbolException, WrongParametersException, RWertException:
{
	int pCounter = 1;
}
{
	expression(method) 
	{
		pCounter += optExpression(method);
		if(pCounter != expectedParamCount)
		{
			throw new WrongParametersException(calledProc);
		}
	}
	| {}
}

int optExpression(Method method) throws UnknownSymbolException, RWertException, WrongParametersException :
{
	int pCounter = 0;
}
{
	<COMMA> expression(method) 
	{
		pCounter += optExpression(method); 
		return (pCounter+1);
	}
	| {return pCounter;}
}

void condition(Method method) throws UnknownSymbolException, RWertException, WrongParametersException:
{
	String op = "";
}
{
	expression(method) <COMPOP> { op = token.image; } expression(method)
	{
		switch (op) {
			case "==":
				method.addCode(ByteCode.IF_ICMPNE.code);
				break;
			case "<":
				method.addCode(ByteCode.IF_ICMPGE.code);
				break;
			case ">":
				method.addCode(ByteCode.IF_ICMPLE.code);
				break;
			case "!=":
				method.addCode(ByteCode.IF_ICMPEQ.code);
				break;
			case "<=":
				method.addCode(ByteCode.IF_ICMPGT.code);
				break;
			case ">=":
				method.addCode(ByteCode.IF_ICMPLT.code);
				break;
			default:
				System.out.println("COMPOP nicht lesbar");
		}
		method.addByteCount(1);
	}
}

void statement(Method method) throws UnknownSymbolException, LWertException, RWertException, WrongParametersException:
{
	int startStmt, endStmt, startChar, ifOffset = 3, startCond, whileOffset = 6;
	String id = "";
}
{
	<IDENT>
	{
		id = token.image;
		idCall(method, id);
	}
	|<PRINT> <BRACKETAUF> expression(method)
	{
		addFunProcCall(method, "print");
	} 
	<BRACKETZU> <SEMICOLON>
	|<CBRACKETAUF> stmtLIST(method) <CBRACKETZU>
	|<IF> condition(method) 
	{
		startStmt = method.getByteCount();
		startChar = method.codeLength();
		statement(method);
		endStmt = method.getByteCount();
		optElse(method);
		if(endStmt != method.getByteCount())
			ifOffset += 3;
		method.insertCode(startChar ,toJmpCount((endStmt - startStmt) + ifOffset));
		method.addByteCount(2);
	} 
	|<WHILE> 
	{
		startCond = method.getByteCount();
		condition(method);	
		startStmt = method.getByteCount();
		startChar = method.codeLength();
		statement(method);
		method.insertCode(startChar, toJmpCount((method.getByteCount() - startStmt) + whileOffset));
		method.addByteCount(2);
		addKombiBefehl(method, ByteCode.GOTO.code, toJmpCount(startCond - method.getByteCount()), 3);
	} 
}

void idCall(Method method, String id) throws UnknownSymbolException, LWertException, RWertException, WrongParametersException:
{
	Tuple<Boolean, String> tup;
	Boolean helpMain;
	Method calledProc;
}
{
	<BRACKETAUF> 
	{
		calledProc = methodList.get(id);
		procCall(method, id, calledProc.getParamCount());
	}
	<BRACKETZU> <SEMICOLON>
	{
		addFunProcCall(method, id);
	}
	|
	<EQUALS> expression(method) <SEMICOLON>
	{
		helpMain = method.isMain();
		try{
			tup = method.getSymbol(id);
		} catch(UnknownSymbolException e){
			Method m = methodList.get("main");
			tup = m.getSymbol(id);
			helpMain = true;
		}
		if(tup.x)
		{
			if(helpMain)
			{
				addKombiBefehl(method, ByteCode.PUTSTATIC.code, "[" + id + "]", 3);
			}else  {
				addKombiBefehl(method, ByteCode.ISTORE.code, toHex(tup.y), 2);
			}
		}else {
			throw new LWertException(id);
		}
	}
}

void optElse(Method method) throws UnknownSymbolException, LWertException, RWertException, WrongParametersException:
{
	int startByte, endByte, startChar, gotoOffset = 3;
}
{
	<ELSE> 
	{
		method.addCode(ByteCode.GOTO.code);
		method.addByteCount(1);

		startByte = method.getByteCount();
		startChar = method.codeLength();
		statement(method);
		endByte = method.getByteCount();
		method.insertCode(startChar, toJmpCount((endByte - startByte) + gotoOffset));
		method.addByteCount(2);
	}
	| {}
}

void stmtLIST(Method method) throws UnknownSymbolException, LWertException, RWertException, WrongParametersException:
{}
{
	statement(method) stmtLIST(method) | {}
}