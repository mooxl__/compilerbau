package p3;

public class RWertException extends Exception{
	public RWertException(String id) {
		super("Symbol (" + id + ") does not return a Value.");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}