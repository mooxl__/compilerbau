package p3;
import java.util.HashMap;
import java.util.Map;

public class SymbolTable {

	Map<String, Tuple<Boolean, String>> symbolMap = new HashMap<>();
	int varCount = 0;

	public SymbolTable(Boolean main){
		if(main)
			varCount = 1;
	}
	
	public void addConstant(String id, String wert) throws SymbolAlreadyDefinedException {
		if(symbolMap.containsKey(id)) {
			throw new SymbolAlreadyDefinedException(id);
		}
		symbolMap.put(id, new Tuple<Boolean, String>(false, wert));
	}
	
	public Tuple<Boolean, String> getSymbol(String id) throws UnknownSymbolException {
		Tuple<Boolean, String> val = symbolMap.get(id);
		if(val == null) {
			throw new UnknownSymbolException(id);
		}
		return symbolMap.get(id);
	}

	public String addVariable(String id) throws SymbolAlreadyDefinedException {
		if(symbolMap.containsKey(id)) {
			throw new SymbolAlreadyDefinedException(id);
		}
		symbolMap.put(id, new Tuple<Boolean, String>(true, Integer.toString(varCount)));
		return Integer.toString(varCount++);
	}
}
