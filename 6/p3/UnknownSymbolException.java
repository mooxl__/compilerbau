package p3;

public class UnknownSymbolException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnknownSymbolException(String id) {
		super("Symbol (" + id + ") ist not defined.");
	}
}
