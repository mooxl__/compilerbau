package p3;

public class WrongParametersException extends Exception{
	public WrongParametersException(String id) {
		super("Given Parameters do not match Parameter List of (" + id + ")");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}