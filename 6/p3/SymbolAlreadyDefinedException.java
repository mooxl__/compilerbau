package p3;

public class SymbolAlreadyDefinedException extends Exception {

	public SymbolAlreadyDefinedException(String id) {
		super("The Symbol (" + id + ") is already defined.");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
