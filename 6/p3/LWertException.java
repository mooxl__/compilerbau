package p3;

public class LWertException extends Exception{
	public LWertException(String id) {
		super("You can not assign a new Value to Symbol (" + id + ").");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}