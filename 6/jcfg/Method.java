package jcfg;

import p3.*;

public class Method{
	StringBuilder codeString = new StringBuilder();
	int byteCount, paramCount;
	String name;
	SymbolTable sTable;
	Boolean main = false, hasReturnValue = false;

	public Method(String name, int paramCount){
		this.name = name;
		this.paramCount = paramCount;
		InitSymbolTable();
	}

	public Method(String name, Boolean hasReturnValue){
		this.name = name;
		this.hasReturnValue = hasReturnValue;
		InitSymbolTable();
	}

	public Method(String name){
		this.name = name;
		InitSymbolTable();
	}

	public Boolean hasReturnValue(){
		return this.hasReturnValue;
	}

	public Boolean isMain(){
		return main;
	}

	private void InitSymbolTable()
	{
		if(this.name.equals("main"))
			main = true;
		sTable = new SymbolTable(main);
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return this.name;
	}

	public int codeLength(){
		return codeString.length();
	}

	public void addCode(String code){
		this.codeString.append(code);
	}

	public String getCode(){
		return codeString.toString();
	}

	public void addByteCount(int toAdd){
		this.byteCount += toAdd;
	}

	public int getByteCount(){
		return byteCount;
	}

	public void addParam(){
		this.paramCount++;
	}

	public void setParamCount(int count){
		this.paramCount = count;
	}

	public int getParamCount(){
		return this.paramCount;
	}

	public MethodObject getMethodObject(){
		return new MethodObject(this.name, this.paramCount, this.codeString.toString());
	}

	public String addVariable(String id) throws SymbolAlreadyDefinedException 
	{
		return sTable.addVariable(id);
	}

	public void addConstant(String id, String value) throws SymbolAlreadyDefinedException
	{
		sTable.addConstant(id, value);
	}

	public Tuple<Boolean, String> getSymbol(String id) throws UnknownSymbolException
	{
		return sTable.getSymbol(id);
	}

	public void insertCode(int index, String text){
		codeString.insert(index, text);
	}
}