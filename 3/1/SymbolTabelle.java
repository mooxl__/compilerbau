import java.util.HashMap;

public class SymbolTabelle {
        
    HashMap<String,String> symbole = new HashMap<String,String>();
    
    public void AddKonstante(String key, String value) {
        symbole.put(key, value);
    }
    public String GetKonstante(String key)  {
        return symbole.get(key);
    }
}