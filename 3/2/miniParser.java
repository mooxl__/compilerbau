/* miniParser.java */
/* Generated By:JavaCC: Do not edit this line. miniParser.java */
    public class miniParser implements miniParserConstants {
        public static void main (String args[]) {
            miniParser parser= new miniParser (System.in);
            try {
                parser.start();
            } catch (ParseException e) {
                System.err.println(e);
            }
        }

  static final public void start() throws ParseException {
    programm();
System.out.println("AUSDRUCK IST OK");
  }

  static final public void programm() throws ParseException {
    constDecl();
    varDecl();
    statement();
  }

  static final public void constDecl() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case FINAL:{
      jj_consume_token(FINAL);
      jj_consume_token(INT);
      constZuw();
      constList();
      jj_consume_token(14);
      break;
      }
    default:
      jj_la1[0] = jj_gen;
      ;
    }
  }

  static final public void constZuw() throws ParseException {
    jj_consume_token(IDENT);
    jj_consume_token(15);
    jj_consume_token(NUMBER);
  }

  static final public void constList() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 16:{
      jj_consume_token(16);
      constZuw();
      constList();
      break;
      }
    default:
      jj_la1[1] = jj_gen;
      ;
    }
  }

  static final public void varDecl() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case INT:{
      jj_consume_token(INT);
      jj_consume_token(IDENT);
      varZuw();
      varList();
      jj_consume_token(14);
      break;
      }
    default:
      jj_la1[2] = jj_gen;
      ;
    }
  }

  static final public void varZuw() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 15:{
      jj_consume_token(15);
      jj_consume_token(NUMBER);
      break;
      }
    default:
      jj_la1[3] = jj_gen;
      ;
    }
  }

  static final public void varList() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 16:{
      jj_consume_token(16);
      jj_consume_token(IDENT);
      varZuw();
      varList();
      break;
      }
    default:
      jj_la1[4] = jj_gen;
      ;
    }
  }

  static final public void expression() throws ParseException {
    term();
    summe();
  }

  static final public void summe() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 17:
    case 18:{
      switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
      case 17:{
        jj_consume_token(17);
        term();
        summe();
        break;
        }
      case 18:{
        jj_consume_token(18);
        term();
        summe();
        break;
        }
      default:
        jj_la1[5] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
      break;
      }
    default:
      jj_la1[6] = jj_gen;
      ;
    }
  }

  static final public void term() throws ParseException {
    faktor();
    produkt();
  }

  static final public void produkt() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case 19:
    case 20:{
      switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
      case 19:{
        jj_consume_token(19);
        term();
        summe();
        break;
        }
      case 20:{
        jj_consume_token(20);
        term();
        summe();
        break;
        }
      default:
        jj_la1[7] = jj_gen;
        jj_consume_token(-1);
        throw new ParseException();
      }
      break;
      }
    default:
      jj_la1[8] = jj_gen;
      ;
    }
  }

  static final public void faktor() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case NUMBER:{
      jj_consume_token(NUMBER);
      break;
      }
    case IDENT:{
      jj_consume_token(IDENT);
      break;
      }
    case 21:{
      jj_consume_token(21);
      expression();
      jj_consume_token(22);
      break;
      }
    default:
      jj_la1[9] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
  }

  static final public void condition() throws ParseException {
    expression();
    jj_consume_token(COMPOP);
    expression();
  }

  static final public void statement() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case IDENT:{
      jj_consume_token(IDENT);
      jj_consume_token(15);
      expression();
      jj_consume_token(14);
      break;
      }
    case PRINT:{
      jj_consume_token(PRINT);
      jj_consume_token(21);
      expression();
      jj_consume_token(22);
      jj_consume_token(14);
      break;
      }
    case 23:{
      jj_consume_token(23);
      stmtLIST();
      jj_consume_token(24);
      break;
      }
    case IF:{
      jj_consume_token(IF);
      condition();
      statement();
      optElse();
      break;
      }
    case WHILE:{
      jj_consume_token(WHILE);
      condition();
      statement();
      break;
      }
    default:
      jj_la1[10] = jj_gen;
      jj_consume_token(-1);
      throw new ParseException();
    }
  }

  static final public void optElse() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case ELSE:{
      jj_consume_token(ELSE);
      statement();
      break;
      }
    default:
      jj_la1[11] = jj_gen;
      ;
    }
  }

  static final public void stmtLIST() throws ParseException {
    switch ((jj_ntk==-1)?jj_ntk_f():jj_ntk) {
    case PRINT:
    case IF:
    case WHILE:
    case IDENT:
    case 23:{
      statement();
      stmtLIST();
      break;
      }
    default:
      jj_la1[12] = jj_gen;
      ;
    }
  }

  static private boolean jj_initialized_once = false;
  /** Generated Token Manager. */
  static public miniParserTokenManager token_source;
  static SimpleCharStream jj_input_stream;
  /** Current token. */
  static public Token token;
  /** Next token. */
  static public Token jj_nt;
  static private int jj_ntk;
  static private int jj_gen;
  static final private int[] jj_la1 = new int[13];
  static private int[] jj_la1_0;
  static {
      jj_la1_init_0();
   }
   private static void jj_la1_init_0() {
      jj_la1_0 = new int[] {0x100,0x10000,0x80,0x8000,0x10000,0x60000,0x60000,0x180000,0x180000,0x202020,0x802e00,0x1000,0x802e00,};
   }

  /** Constructor with InputStream. */
  public miniParser(java.io.InputStream stream) {
     this(stream, null);
  }
  /** Constructor with InputStream and supplied encoding */
  public miniParser(java.io.InputStream stream, String encoding) {
    if (jj_initialized_once) {
      System.out.println("ERROR: Second call to constructor of static parser.  ");
      System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
      System.out.println("       during parser generation.");
      throw new Error();
    }
    jj_initialized_once = true;
    try { jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source = new miniParserTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 13; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  static public void ReInit(java.io.InputStream stream) {
     ReInit(stream, null);
  }
  /** Reinitialise. */
  static public void ReInit(java.io.InputStream stream, String encoding) {
    try { jj_input_stream.ReInit(stream, encoding, 1, 1); } catch(java.io.UnsupportedEncodingException e) { throw new RuntimeException(e); }
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 13; i++) jj_la1[i] = -1;
  }

  /** Constructor. */
  public miniParser(java.io.Reader stream) {
    if (jj_initialized_once) {
      System.out.println("ERROR: Second call to constructor of static parser. ");
      System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
      System.out.println("       during parser generation.");
      throw new Error();
    }
    jj_initialized_once = true;
    jj_input_stream = new SimpleCharStream(stream, 1, 1);
    token_source = new miniParserTokenManager(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 13; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  static public void ReInit(java.io.Reader stream) {
    jj_input_stream.ReInit(stream, 1, 1);
    token_source.ReInit(jj_input_stream);
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 13; i++) jj_la1[i] = -1;
  }

  /** Constructor with generated Token Manager. */
  public miniParser(miniParserTokenManager tm) {
    if (jj_initialized_once) {
      System.out.println("ERROR: Second call to constructor of static parser. ");
      System.out.println("       You must either use ReInit() or set the JavaCC option STATIC to false");
      System.out.println("       during parser generation.");
      throw new Error();
    }
    jj_initialized_once = true;
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 13; i++) jj_la1[i] = -1;
  }

  /** Reinitialise. */
  public void ReInit(miniParserTokenManager tm) {
    token_source = tm;
    token = new Token();
    jj_ntk = -1;
    jj_gen = 0;
    for (int i = 0; i < 13; i++) jj_la1[i] = -1;
  }

  static private Token jj_consume_token(int kind) throws ParseException {
    Token oldToken;
    if ((oldToken = token).next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    if (token.kind == kind) {
      jj_gen++;
      return token;
    }
    token = oldToken;
    jj_kind = kind;
    throw generateParseException();
  }


/** Get the next Token. */
  static final public Token getNextToken() {
    if (token.next != null) token = token.next;
    else token = token.next = token_source.getNextToken();
    jj_ntk = -1;
    jj_gen++;
    return token;
  }

/** Get the specific Token. */
  static final public Token getToken(int index) {
    Token t = token;
    for (int i = 0; i < index; i++) {
      if (t.next != null) t = t.next;
      else t = t.next = token_source.getNextToken();
    }
    return t;
  }

  static private int jj_ntk_f() {
    if ((jj_nt=token.next) == null)
      return (jj_ntk = (token.next=token_source.getNextToken()).kind);
    else
      return (jj_ntk = jj_nt.kind);
  }

  static private java.util.List<int[]> jj_expentries = new java.util.ArrayList<int[]>();
  static private int[] jj_expentry;
  static private int jj_kind = -1;

  /** Generate ParseException. */
  static public ParseException generateParseException() {
    jj_expentries.clear();
    boolean[] la1tokens = new boolean[25];
    if (jj_kind >= 0) {
      la1tokens[jj_kind] = true;
      jj_kind = -1;
    }
    for (int i = 0; i < 13; i++) {
      if (jj_la1[i] == jj_gen) {
        for (int j = 0; j < 32; j++) {
          if ((jj_la1_0[i] & (1<<j)) != 0) {
            la1tokens[j] = true;
          }
        }
      }
    }
    for (int i = 0; i < 25; i++) {
      if (la1tokens[i]) {
        jj_expentry = new int[1];
        jj_expentry[0] = i;
        jj_expentries.add(jj_expentry);
      }
    }
    int[][] exptokseq = new int[jj_expentries.size()][];
    for (int i = 0; i < jj_expentries.size(); i++) {
      exptokseq[i] = jj_expentries.get(i);
    }
    return new ParseException(token, exptokseq, tokenImage);
  }

  /** Enable tracing. */
  static final public void enable_tracing() {
  }

  /** Disable tracing. */
  static final public void disable_tracing() {
  }

    }
